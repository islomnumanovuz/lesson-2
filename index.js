"use strict";
// 1. Write a program that is divisible by 3 and 5
function divisible(number){
    if(typeof number == "string") alert("Please enter a number");
    console.log(`User entered ${number}`);
    for(let i = 1; i <= number; i++){
        if(i % 3 === 0 && i % 5 === 0) console.log(`${i} = divisible by 3 and 5`);
        else if(i % 3 === 0) console.log(`${i} = divisible by 3`);
        else if (i % 5 === 0) console.log(`${i} divisible by 5`);
    }
};
divisible(15);
//--------------------------------------------------------------
// 2. Write a program that checks whether given number odd or even.
function showNumber(number){
    if(typeof number == "string") alert("Please enter a number");
    console.log(`User entered ${number}`);
    for(let i = 1; i <= number; i++){
        if(i % 2 === 0) console.log(`${i} is EVEN`);
        else console.log(`${i} is ODD`);
    }
};
showNumber(100);
//--------------------------------------------------------------
// 3. Write a program that get array as argument and sorts it by order // [1,2,3,4...];
let array = [2, 5, 11, 3, 4, 10, 9, 7, 6, 1, 8];
array.sort(function(a,b){
    return a - b
});
console.log(array);
//------------------------------------------------